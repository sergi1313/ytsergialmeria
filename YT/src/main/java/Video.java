import javax.annotation.processing.SupportedSourceVersion;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class Video {
    private int megusta;
    private Video video;
    static ArrayList<Comentario> comentarios;
    private static String nombre;
    private static LocalDate fecha;
    int opcion;
    public Video(String nombre) {
		setNumeroLikes(getNumeroLikes() + 1);
        setNombre(nombre);
        setFecha(LocalDate.now());
        comentarios = new ArrayList<Comentario>();
    }

    public void menu(){
        opcion = menuVideo();
        while (opcion != 0){
            switch (opcion){
                case 1:
                    nuevoComentario();
                    break;
                case 2:
                    like();
                    break;
                case 3:
                    mostrarComentarios();
                    break;
                case 4:
                    mostrarEstadisticas();
                    break;
            }
        opcion = menuVideo();

        }
    }

    private int menuVideo(){
        Scanner leer = new Scanner(System.in);

        System.out.println();
        System.out.println("|---" + getNombre() + "---|");
        System.out.println("1. Nuevo comentario");
        System.out.println("2. Like");
        System.out.println("3. Mostrar comentarios");
        System.out.println("4. Mostrar estadísticas");
        System.out.println("0. Salir");
        System.out.println("|--------------------|");
        int opcion = leer.nextInt();
        return opcion;
    }

    private void nuevoComentario(){
        Scanner leer = new Scanner(System.in);
        System.out.println("introduce comentario: ");
        String frase = leer.nextLine();
        System.out.println("Introduce nombre de usuario: ");
        String usuario = leer.nextLine();
        Comentario comentario = new Comentario(frase,usuario);
        comentarios.add(comentario);

    }

    private void like(){
        megusta++;
        System.out.println("ME GUSTA");
    }

    private static  void mostrarComentarios(){
        for (int i = 0; i < comentarios.size(); i++){
        	Comentario imprimirComentario = comentarios.get(i);
        	System.out.println("Comentario: " +imprimirComentario.getTextoComentario()+ "del usuario: " +imprimirComentario.getUsuario()+ "en fecha: "+imprimirComentario.getFechaComentario());
        }


    }
    public ArrayList<Comentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(ArrayList<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

    private void mostrarEstadisticas(){
    	System.out.println("Video con nombre; " +getNombre() + "con fecha: " +getFecha()+ "con likes : "+megusta+" con estos comentarios: "+comentarios.size());
    }
    
    public void setNombre(String nombre)
    {this.nombre = nombre;
    }
    public String getNombre()
    {return nombre;
    }
    public void setFecha(LocalDate fecha)
    {this.fecha = fecha;
    }
    public LocalDate getFecha()
    { return fecha;
    }
    public int getNumeroLikes() {
		return megusta;
	}

	public void setNumeroLikes(int numeroLikes) {
		this.megusta = numeroLikes;
	}
}