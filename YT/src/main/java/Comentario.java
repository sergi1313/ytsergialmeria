
import java.util.Date;

public class Comentario {

	private String usuario;
	private String textoComentario;
	private Date fechaComentario;
	
	public Comentario(String usuario, String textoComentario) {
		setUsuario(usuario);
		setTextoComentario(textoComentario);
		setFechaComentario(new Date());
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTextoComentario () {
		return textoComentario;
	}
	public void setTextoComentario (String textoComentario) {
		this.textoComentario = textoComentario;
	}
	public Date getFechaComentario () {
		return fechaComentario;
	}
	public void setFechaComentario (Date date) {
		this.fechaComentario = date;
	}
}