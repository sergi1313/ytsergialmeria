import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class Canal {
    static ArrayList<Video> videos;
    private static Canal canal;
    private static String nombre;
    private static LocalDate fecha;

    public Canal (String nombre){
        setNombre(nombre);
        setFecha(LocalDate.now());
        videos = new ArrayList<Video>();

    }

    public void menu() {
        Scanner leer = new Scanner(System.in);
        int opcion = 5;
        while (opcion != 0) {
            menuCanal();
            opcion = leer.nextInt();
            switch (opcion) {
                case 1:
                    nuevoVideo();
                    break;
                case 2:
                    seleccionarVideo();
                    break;
                case 3:
                    mostrarEstCan();
                    break;
                case 4:
                    mostrarInfovideo();
                    break;
                default:
            }
            
        }
    }

     private void menuCanal(){
        System.out.println();
        System.out.println("|---" + getNombre() + "---|" );
        System.out.println("1. Nuevo video");
        System.out.println("2. Seleccionar video");
        System.out.println("3. Mostrar estadisticas");
        System.out.println("4. Mostrar info videos");
        System.out.println("0. Salir");
    }

    private static void nuevoVideo(){
        Scanner leer = new Scanner(System.in);

        System.out.println("Introduce el nombre del video: ");
        String nombre = leer.nextLine();
        Video video = new Video (nombre);
        videos.add(video);
        video.menu();

    }

    private static void seleccionarVideo(){
        Scanner leer =  new Scanner(System.in);
        int opcion;

        if (videos.size() == 0){
        	System.out.println("No hay videos.");
            nuevoVideo();
        } else {
            System.out.println();
            System.out.println("Selecciona el video: ");
            Video video;
            for (int i = 0; i < videos.size(); i++) {
                video = videos.get(i);
                System.out.println((i) +"El video " + video.getNombre() + " con fecha: " + video.getFecha());
            }
            opcion = leer.nextInt();
            video = videos.get(opcion);
            video.menu();
        }

    }

    private static void mostrarEstCan(){
    	System.out.println(
				"Nombre del canal: " + getNombre() + " en fecha" + getFecha() + " con " + videos.size() + " videos");
	}
    private static void mostrarInfovideo(){
    	 for (int i = 0; i < videos.size(); i++) {
             Video imprimirVideo = videos.get(i);
             System.out.println((i) +"El video " + imprimirVideo.getNombre() + " con fecha: " + imprimirVideo.getFecha()+ "con likes: " +imprimirVideo.getNumeroLikes()+" con la siguiente cantidad de comentarios: "+imprimirVideo.comentarios.size());
         }
    }

    public static String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
				
    public void setFecha(LocalDate fecha )
    {this.fecha = fecha;
    }
    public static LocalDate getFecha()
    {return fecha;
    }

}
