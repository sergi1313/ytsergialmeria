import java.util.ArrayList;
import java.util.Scanner;

public class Youtube {
	static ArrayList<Canal> canales;

	public static void main(String[] args) {
		int opcion = menuYoutube();
		canales = new ArrayList<Canal>();

		while (opcion != 5) {
			switch (opcion) {
			case 1:
				nuevoCanal();
				break;
			case 2:
				seleccionarCanal();
				break;
			case 3:
				mostrarEstadisticas();
				break;
			case 4:
				mostrarEstadisticasTotales();
				break;
			default:
				System.out.println("Introduce valor válido para continuar: ");
			}
			opcion = menuYoutube();
		}
		System.out.println("QUE VAYA BIEN!!");

	}

	static int menuYoutube() {
		Scanner leer = new Scanner(System.in);
		System.out.println();
		System.out.println("|---YOUTUBE---|");
		System.out.println("1. Nuevo Canal");
		System.out.println("2. Seleccionar Canal");
		System.out.println("3. Mostrar estadisticas");
		System.out.println("4. Mostrar estadisticas completas");
		System.out.println("5. Salir");
		System.out.println("|----------------------------|");
		int opcion = leer.nextInt();
		return opcion;

	}

	private static void nuevoCanal() {
		Scanner leer = new Scanner(System.in);

		System.out.println("Introduce el nombre del canal: ");
		String nombre = leer.nextLine();
		Canal canal = new Canal(nombre);
		canales.add(canal);
		canal.menu();

	}

	private static void seleccionarCanal() {
		Scanner leer = new Scanner(System.in);
		int opcion;

		if (canales.size() == 0) {
			nuevoCanal();
		} else {
			System.out.println("Selecciona Canal");
			Canal canal;
			for (int i = 0; i < canales.size(); i++) {
				canal = canales.get(i);
				System.out.println((i) + "- El canal " + canal.getNombre() + " creado en fecha: " + canal.getFecha());
			}
			opcion = leer.nextInt();
			canal = canales.get(opcion);
			canal.menu();
		}

	}

	private static void mostrarEstadisticas() {
		Canal canal;
		for (int i = 0; i < canales.size(); i++) {
			canal = canales.get(i);
			System.out.println((i + 1) + "- El canal " + canal.getNombre() + " creado en fecha: " + canal.getFecha()
					+ " con los siguientes videos: " + canal.videos);
		}

	}

	private static void mostrarEstadisticasTotales() {
		System.out.println("Youtube tiene " + canales.size() + " canales");
		Canal estadisticasCanal;
		for (int i = 0; i < canales.size(); i++) {
			estadisticasCanal = canales.get(i);
			System.out.println(i + "-Nombre del canal: " + estadisticasCanal.getNombre() + " creado en fecha: "
					+ estadisticasCanal.getFecha());
			for (int j = 0; j < videos.size(); j++) {
				Video imprimirVideo = videos.get(j);
				System.out.println((j) + "El video " + imprimirVideo.getNombre() + " con fecha: "
						+ imprimirVideo.getFecha() + "con likes: " + imprimirVideo.getNumeroLikes()
						+ " con la siguiente cantidad de comentario");
				 for (int k = 0; k < comentarios.size(); k++){
			        	Comentario imprimirComentario = comentarios.get(k);
			        	System.out.println("Comentario: " +imprimirComentario.getTextoComentario()+ "del usuario: " +imprimirComentario.getUsuario()+ "en fecha: "+imprimirComentario.getFechaComentario());
			        }
			}
		}

	}

	static ArrayList<Video> videos;
	 static ArrayList<Comentario> comentarios;
}
